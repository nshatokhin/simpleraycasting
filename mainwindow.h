#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    void init();

    void updateMethod(float dt);

    void mainLoop();

private:
    Ui::MainWindow *ui;

    static constexpr qint8 pixelWidth = 5;
    static constexpr qint8 pixelHeight = 5;

    static constexpr qint64 MS_PER_UPDATE = 1000 / 60;
    static constexpr float MS_PER_UPDATE_F = 1.0f / 60;

    qint64 nScreenWidth; // Ширина окна
    qint64 nScreenHeight; // Высота окна
    qint64 mapWidth, mapHeight, mapBorder;

    float fPlayerX = 1.0f; // Координата игрока по оси X
    float fPlayerY = 1.0f; // Координата игрока по оси Y
    float fPlayerA = 0.0f; // Направление игрока

    int nMapHeight = 16; // Высота игрового поля
    int nMapWidth = 16;  // Ширина игрового поля

    float fFOV = 3.14159f / 3; // Угол обзора (поле видимости)
    float fDepth = 30.0f;     // Максимальная дистанция обзора

    QString map; //карта

    qint64 prevTime, currentTime, lag;

    bool keyA, keyD, keyW, keyS;

private:

    void paintEvent(QPaintEvent *event);
    void keyPressEvent(QKeyEvent *event);
    void keyReleaseEvent(QKeyEvent *event);
};

#endif // MAINWINDOW_H
