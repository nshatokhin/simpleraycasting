#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <cmath>

#include <vector>

#include <QColor>
#include <QDateTime>
#include <QKeyEvent>
#include <QPainter>
#include <QRgb>
#include <QTimer>

float rotateVec(double &x1, double &y1, double x, double y, double angle)
{
    x1=x*cos(angle)-y*sin(angle);
    y1=y*cos(angle)+x*sin(angle);
}

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    nScreenWidth(256),
    nScreenHeight(144),
    mapBorder(3),
    mapWidth(300),
    mapHeight(300),
    fPlayerX(1.0f),
    fPlayerY(1.0f),
    fPlayerA(0.0f),
    nMapHeight(16),
    nMapWidth(16),
    fFOV(3.14159f / 3),
    fDepth(30.0f),
    map(),
    lag(0),
    keyA(false), keyD(false), keyW(false), keyS(false)
{
    ui->setupUi(this);

    prevTime = QDateTime::currentMSecsSinceEpoch();

    setFixedSize(nScreenWidth * pixelWidth, nScreenHeight * pixelHeight);

    QTimer::singleShot(MS_PER_UPDATE, this, &MainWindow::mainLoop);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::init()
{
    map.clear();
    map += "################";
    map += "#..............#";
    map += "#..............#";
    map += "#..............#";
    map += "#..............#";
    map += "#..............#";
    map += "#......##......#";
    map += "#..............#";
    map += "#......##......#";
    map += "#..............#";
    map += "#..............#";
    map += "#..............#";
    map += "#....#...#.....#";
    map += "#...##...##....#";
    map += "#...##...##....#";
    map += "################";


}

void MainWindow::updateMethod(float dt)
{
    if( keyA ) {
        fPlayerA -= (1.5f) * dt; // Клавишей "A" поворачиваем по часовой стрелке
    }

    if( keyD ) {
        fPlayerA += (1.5f) * dt; // Клавишей "D" поворачиваем против часовой стрелки
    }

    if( keyW ) {
        fPlayerX += sinf(fPlayerA) * 5.0f * dt;
        fPlayerY += cosf(fPlayerA) * 5.0f * dt;

        if (map[(int)fPlayerY*nMapWidth + (int)fPlayerX] == '#') { // Если столкнулись со стеной, но откатываем шаг
            fPlayerX -= sinf(fPlayerA) * 5.0f * dt;
            fPlayerY -= cosf(fPlayerA) * 5.0f * dt;
        }
    }

    if( keyS )
    {
        fPlayerX -= sinf(fPlayerA) * 5.0f * dt;
        fPlayerY -= cosf(fPlayerA) * 5.0f * dt;

        if (map[(int)fPlayerY*nMapWidth + (int)fPlayerX] == '#')
        { // Если столкнулись со стеной, но откатываем шаг
            fPlayerX += sinf(fPlayerA) * 5.0f * dt;
            fPlayerY += cosf(fPlayerA) * 5.0f * dt;
        }
    }
}

void MainWindow::mainLoop()
{
    currentTime = QDateTime::currentMSecsSinceEpoch();
    qint64 elapsed = currentTime - prevTime;
    prevTime = currentTime;
    lag += elapsed;

    while (lag >= MS_PER_UPDATE)
    {
        updateMethod(MS_PER_UPDATE_F);
        lag -= MS_PER_UPDATE;
    }

    repaint();

    QTimer::singleShot(lag, this, &MainWindow::mainLoop);
}

void MainWindow::paintEvent(QPaintEvent *)
{
    QPainter painter(this);



    for (int x = 0; x < nScreenWidth; x++)  // Проходим по всем X
    {
        float fRayAngle = (fPlayerA - fFOV/2.0f) + ((float)x / (float)nScreenWidth) * fFOV; // Направление луча

        float fDistanceToWall = 0.0f; // Расстояние до препятствия в направлении fRayAngle
        bool bHitWall = false, bBoundary = false; // Достигнул ли луч стенку

        float fEyeX = sinf(fRayAngle); // Координаты единичного вектора fRayAngle
        float fEyeY = cosf(fRayAngle);

        while (!bHitWall && fDistanceToWall < fDepth) // Пока не столкнулись со стеной
        {                                                  // Или не вышли за радиус видимости
            fDistanceToWall += 0.1f;

            int nTestX = (int)(fPlayerX + fEyeX*fDistanceToWall); // Точка на игровом поле
            int nTestY = (int)(fPlayerY + fEyeY*fDistanceToWall); // в которую попал луч

            if (nTestX < 0 || nTestX >= nMapWidth || nTestY < 0 || nTestY >= nMapHeight)
            { // Если мы вышли за карту, то дальше смотреть нет смысла - фиксируем соударение на расстоянии видимости
                bHitWall = true;
                fDistanceToWall = fDepth;
            }
            else if (map[nTestY*nMapWidth + nTestX] == '#')
            { // Если встретили стену, то заканчиваем цикл
                bHitWall = true;

                std::vector <std::pair <float, float>> p;

                for (int tx = 0; tx < 2; tx++)
                    for (int ty = 0; ty < 2; ty++) // Проходим по всем 4м рёбрам
                    {
                        float vx = (float)nTestX + tx - fPlayerX; // Координаты вектора,
                        float vy = (float)nTestY + ty - fPlayerY; // ведущего из наблюдателя в ребро
                        float d = sqrt(vx*vx + vy*vy); // Модуль этого вектора
                        float dot = (fEyeX*vx / d) + (fEyeY*vy / d); // Скалярное произведение (единичных векторов)
                        p.push_back(std::make_pair(d, dot)); // Сохраняем результат в массив
                    }
                // Мы будем выводить два ближайших ребра, поэтому сортируем их по модулю вектора ребра
                sort(p.begin(), p.end(), [](const std::pair <float, float> &left, const std::pair <float, float> &right) {return left.first < right.first; });

                float fBound = 0.005; // Угол, при котором начинаем различать ребро.
                if (acos(p.at(0).second) < fBound) bBoundary = true;
                if (acos(p.at(1).second) < fBound) bBoundary = true;
            }
        }

        //Вычисляем координаты начала и конца стенки по формулам (1) и (2)
        int nCeiling = (float)(nScreenHeight/2.0) - nScreenHeight / ((float)fDistanceToWall);
        int nFloor = nScreenHeight - nCeiling;

        QColor shadeColor;

        if (fDistanceToWall <= fDepth / 3.0f)        shadeColor = QColor::fromRgb(230, 230, 230); // Если стенка близко, то рисуем
        else if (fDistanceToWall < fDepth / 2.0f)    shadeColor = QColor::fromRgb(200, 200, 200); // светлую полоску
        else if (fDistanceToWall < fDepth / 1.5f)    shadeColor = QColor::fromRgb(100, 100, 100); // Для отдалённых участков
        else if (fDistanceToWall < fDepth)           shadeColor = QColor::fromRgb(50, 50, 50); // рисуем более темную
        else                                         shadeColor = QColor::fromRgb(0, 0, 0);

        if (bBoundary)		shadeColor = QColor::fromRgb(0, 0, 0); // Рисуем черную вертикальную черту, если это грань

        QColor pixelColor;
        for (int y = 0; y < nScreenHeight; y++)
        {
            if (y <= nCeiling)
            {
                pixelColor = QColor::fromRgb(0, 0, 0);
            }
            else if(y > nCeiling && y <= nFloor)
                pixelColor = shadeColor;
            else
            {
                // То же самое с полом - более близкие части рисуем более заметными символами
                float b = 1.0f - ((float)y - nScreenHeight / 2.0) / ((float)nScreenHeight / 2.0);
                if (b < 0.25)        pixelColor = QColor::fromRgb(0, 230, 0);
                else if (b < 0.5)    pixelColor = QColor::fromRgb(0, 200, 0);
                else if (b < 0.75)   pixelColor = QColor::fromRgb(0, 100, 0);
                else if (b < 0.9)    pixelColor = QColor::fromRgb(0, 50, 0);
                else                 pixelColor = QColor::fromRgb(0, 0, 0);
            }

            painter.fillRect(QRect(x*pixelWidth, y*pixelHeight, pixelWidth, pixelHeight), pixelColor);
            painter.setPen(pixelColor);
            //painter.drawPoint(x, y);
        }

        // draw map

        int mapOffsetX = width() - mapWidth + mapBorder;
        int mapOffsetY = mapBorder;

        painter.fillRect(QRect(mapOffsetX, mapOffsetY, mapWidth - mapBorder * 2, mapHeight - mapBorder * 2), QColor::fromRgb(0, 0, 0));

        int mapCellWidth = mapWidth / nMapWidth;
        int mapCellHeight = mapHeight / nMapHeight;

        for(int i=0;i<nMapWidth;i++) {
            for(int j=0;j<nMapHeight;j++) {
                if( map[j * nMapWidth + i] == '#' ) {
                    painter.fillRect(QRect(mapOffsetX + i*mapCellWidth, mapOffsetY + j*mapCellHeight, mapCellWidth, mapCellHeight), QColor::fromRgb(255, 255, 255));
                }
            }
        }

        painter.setBrush(QBrush(Qt::red));
        painter.drawEllipse(QRect(mapOffsetX + fPlayerX*mapCellWidth - mapCellWidth/2, mapOffsetY + fPlayerY*mapCellHeight - mapCellHeight/2, mapCellWidth, mapCellHeight));

        int startPointX1 = mapOffsetX + fPlayerX*mapCellWidth;
        int startPointY1 = mapOffsetY + fPlayerY*mapCellHeight;

        float leftAngle = (fPlayerA - fFOV/2.0f);
        float rightAngle = (fPlayerA - fFOV/2.0f) + fFOV;

        double endPointX1, endPointY1;
        rotateVec(endPointX1, endPointY1, 0, 100, -leftAngle);
        endPointX1 += startPointX1;
        endPointY1 += startPointY1;

        double endPointX2, endPointY2;
        rotateVec(endPointX2, endPointY2, 0, 100, -rightAngle);
        endPointX2 += startPointX1;
        endPointY2 += startPointY1;

        QPainterPath path;
        // Set pen to this point.
        path.moveTo (startPointX1, startPointY1);
        // Draw line from pen point to this point.
        path.lineTo (endPointX1, endPointY1);

        //path.moveTo (endPointX1, endPointY1); // <- no need to move
        path.lineTo (endPointX2,   endPointY2);

        //path.moveTo (endPointX2,   endPointY2); // <- no need to move
        path.lineTo (startPointX1, startPointY1);

        painter.setPen (Qt :: NoPen);
        painter.fillPath (path, QBrush (QColor::fromRgba(qRgba(255, 255, 0, 50))));
    }
}

void MainWindow::keyPressEvent(QKeyEvent *event)
{
    if( event->key() == Qt::Key_A ) {
        keyA = true;
    }
    else if( event->key() == Qt::Key_D ) {
        keyD = true;
    }
    else if( event->key() == Qt::Key_W ) {
        keyW = true;
    }
    else if( event->key() == Qt::Key_S ) {
        keyS = true;
    }
}

void MainWindow::keyReleaseEvent(QKeyEvent *event)
{
    if( event->key() == Qt::Key_A ) {
        keyA = false;
    }
    else if( event->key() == Qt::Key_D ) {
        keyD = false;
    }
    else if( event->key() == Qt::Key_W ) {
        keyW = false;
    }
    else if( event->key() == Qt::Key_S ) {
        keyS = false;
    }
}
